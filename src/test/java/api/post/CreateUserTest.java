package api.post;

import com.api.data.User;
import com.api.request.Request;
import com.api.response.CustomResponse;
import com.api.response.ResponseUser;
import io.restassured.response.Response;
import org.apache.commons.lang3.RandomUtils;
import org.testng.annotations.Test;

import static com.api.assertion.Comparator.compare;
import static com.api.assertion.ContentType.checkContentType;
import static com.api.assertion.StatusCode.checkStatusCode;
import static com.api.contenttype.ContentType.JSON;
import static com.api.statuscode.StatusCode.OK;
import static com.api.url.UserUrl.CREATE_USER_URL;

public class CreateUserTest {

    @Test(description = "Create new user")
    public void createNewUser() {

        String username = "autotest" + RandomUtils.nextInt();
        String email = "autotest" + RandomUtils.nextInt() + "@gmail.com";
        String password = "autotest" + RandomUtils.nextInt();

        User newUser = new User(username, email, password);

        Response response = Request.doPost(CREATE_USER_URL, newUser);

        ResponseUser createdUserResponse = CustomResponse.getResponseBodyAs(response, ResponseUser.class);

        checkStatusCode(OK, response.getStatusCode());

        checkContentType(JSON, response.getContentType());

        ResponseUser expectedResponse = new ResponseUser()
                .setSuccess(true)
                .setMessage("User Successfully created")
                .setDetails(newUser);

        compare(createdUserResponse, expectedResponse);
    }
}