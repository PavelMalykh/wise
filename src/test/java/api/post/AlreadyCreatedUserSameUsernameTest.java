package api.post;

import com.api.data.User;
import com.api.request.Request;
import com.api.response.CustomResponse;
import com.api.response.ResponseError;
import io.restassured.response.Response;
import org.apache.commons.lang3.RandomUtils;
import org.testng.annotations.Test;

import java.util.List;

import static com.api.assertion.Comparator.compare;
import static com.api.assertion.ContentType.checkContentType;
import static com.api.assertion.StatusCode.checkStatusCode;
import static com.api.contenttype.ContentType.JSON;
import static com.api.statuscode.StatusCode.BAD_REQUEST;
import static com.api.url.UserUrl.CREATE_USER_URL;

public class AlreadyCreatedUserSameUsernameTest {

    //For example, We already know that 'stan' is used username

    @Test(description = "Already created new user (username is already been taken)")
    public void alreadyCreatedUser() {

        String username = "stan";
        String email = "autotest" + RandomUtils.nextInt() + "@gmail.com";
        String password = "autotest" + RandomUtils.nextInt();

        User newUser = new User(username, email, password);

        Response response = Request.doPost(CREATE_USER_URL, newUser);

        ResponseError responseError = CustomResponse.getResponseBodyAs(response, ResponseError.class);

        checkStatusCode(BAD_REQUEST, response.getStatusCode());

        checkContentType(JSON, response.getContentType());

        ResponseError expectedResponse = new ResponseError()
                .setSuccess(false)
                .setMessage(List.of("This username is taken. Try another."));

        compare(responseError, expectedResponse);
    }
}