package api.get;

import com.api.data.User;
import com.api.request.Request;
import com.api.response.CustomResponse;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static com.api.assertion.ContentType.checkContentType;
import static com.api.assertion.Size.collectionSizeShouldBeMoreThan;
import static com.api.assertion.StatusCode.checkStatusCode;
import static com.api.contenttype.ContentType.JSON;
import static com.api.statuscode.StatusCode.OK;
import static com.api.url.UserUrl.GET_USER_URL;

public class GetAllUsers {

    @Test(description = "Get all users")
    public void getAllUsers() {

        Response response = Request.doGet(GET_USER_URL);

        User[] users = CustomResponse.getResponseBodyAs(response, User[].class);

        checkStatusCode(OK, response.getStatusCode());

        checkContentType(JSON, response.getContentType());

        collectionSizeShouldBeMoreThan(users.length, 1);
    }
}