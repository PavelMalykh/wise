package api.get;

import com.api.data.User;
import com.api.request.Request;
import com.api.response.CustomResponse;
import com.api.response.ResponseUser;
import com.api.searchengine.UserSearchEngine;
import io.restassured.response.Response;
import org.apache.commons.lang3.RandomUtils;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.api.assertion.Comparator.compare;
import static com.api.assertion.ContentType.checkContentType;
import static com.api.assertion.StatusCode.checkStatusCode;
import static com.api.contenttype.ContentType.JSON;
import static com.api.searchengine.UserSearchEngine.findUserByUsername;
import static com.api.statuscode.StatusCode.OK;
import static com.api.url.UserUrl.CREATE_USER_URL;
import static com.api.url.UserUrl.GET_USER_URL;

public class GetUserAfterCreationTest {

    private static final String username = "autotest" + RandomUtils.nextInt();
    private static final String email = "autotest" + RandomUtils.nextInt() + "@gmail.com";
    private static final String password = "autotest" + RandomUtils.nextInt();
    private static final User newUser = new User(username, email, password);

    @BeforeTest
    private void createUser() {
        Response response = Request.doPost(CREATE_USER_URL, newUser);

        checkStatusCode(OK, response.getStatusCode());
    }

    @Test(description = "Check that new user will be created by get method")
    public void getUserAfterCreation() {

        Response getResponse = Request.doGet(GET_USER_URL);

        User[] users = CustomResponse.getResponseBodyAs(getResponse, User[].class);

        checkStatusCode(OK, getResponse.getStatusCode());

        User createdUser = findUserByUsername(users, username);

        compare(createdUser, newUser);
    }
}