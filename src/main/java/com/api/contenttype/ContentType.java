package com.api.contenttype;

public enum ContentType {

    JSON("application/json; charset=utf-8");

    public String value;

    private ContentType(String code) {
        this.value = code;
    }
}