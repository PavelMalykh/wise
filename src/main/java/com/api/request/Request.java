package com.api.request;

import com.api.data.User;
import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.response.Response;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class Request {

    @Step("Send GET request url: {url}")
    public static Response doGet(String url) {
        return given().filter(new AllureRestAssured()).get(url);
    }

    @Step("Send api.post request url: {url}")
    public static Response doPost(String url, User user) {
        return given()
                .filter(new AllureRestAssured())
                .formParam("username", user.getUsername())
                .formParam("email", user.getEmail())
                .formParam("password", user.getPassword())
                .post(url);
    }

    @Step("Send GET request url with query params: {url}")
    public static Response doGet(String url, Map<String, Object> params) {
        return given().
                filter(new AllureRestAssured())
                .queryParams(params)
                .get(url);
    }
}