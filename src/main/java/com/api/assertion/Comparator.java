package com.api.assertion;

import io.qameta.allure.Step;
import org.testng.Assert;

public class Comparator {

    @Step("Compare objects")
    public static void compare(Object actualData, Object fixtureData) {
        Assert.assertEquals(actualData, fixtureData,  fixtureData + " is not equal to " + actualData);
    }
}