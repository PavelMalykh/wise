package com.api.assertion;

import io.qameta.allure.Step;
import org.testng.Assert;

public class Size {

    @Step("Collection size: {collectionSize} should be more than: {moreThanSize}")
    public static void collectionSizeShouldBeMoreThan(int collectionSize, int moreThanSize) {
        Assert.assertTrue(collectionSize > moreThanSize, "Collection size: " + collectionSize + " is less than to: " + moreThanSize);
    }
}