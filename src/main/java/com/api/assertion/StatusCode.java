package com.api.assertion;

import io.qameta.allure.Step;

import static org.testng.Assert.assertEquals;

public class StatusCode {

    @Step("Check status code")
    public static void checkStatusCode(com.api.statuscode.StatusCode expectedStatusCode, int actualStatusCode) {
        assertEquals(expectedStatusCode.value, actualStatusCode, actualStatusCode + " is noq equal to: " + expectedStatusCode.value);
    }
}