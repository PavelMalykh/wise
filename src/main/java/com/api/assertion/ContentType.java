package com.api.assertion;

import io.qameta.allure.Step;

import static org.testng.Assert.assertEquals;

public class ContentType {

    @Step("Check content type")
    public static void checkContentType(com.api.contenttype.ContentType expectedContentType, String actualContentType) {
        assertEquals(actualContentType, expectedContentType.value, actualContentType + " is noq equal to: " + expectedContentType.value);
    }
}