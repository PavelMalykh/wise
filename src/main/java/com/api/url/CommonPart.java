package com.api.url;

import static com.api.socketaddress.SocketAddress.host;
import static com.api.socketaddress.SocketAddress.protocol;

public class CommonPart {

    public static final String COMMON_URL_PART = protocol + "://" + host;
}