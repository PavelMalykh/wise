package com.api.url;

import static com.api.endpoint.User.CREATE_USER;
import static com.api.endpoint.User.GET_USER;
import static com.api.url.CommonPart.COMMON_URL_PART;

public class UserUrl {

    public static final String CREATE_USER_URL = COMMON_URL_PART + CREATE_USER;
    public static final String GET_USER_URL = COMMON_URL_PART + GET_USER;
}