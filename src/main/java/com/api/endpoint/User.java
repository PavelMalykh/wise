package com.api.endpoint;

public class User {

    public static final String GET_USER = "/user/get";
    public static final String CREATE_USER = "/user/create";
}