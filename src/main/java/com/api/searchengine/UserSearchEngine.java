package com.api.searchengine;

import com.api.data.User;
import io.qameta.allure.Step;

import java.util.Arrays;

public class UserSearchEngine {

    @Step("Find user by username")
    public static User findUserByUsername(User[] users, String username) {
        return Arrays.stream(users).filter(user -> user.getUsername().equals(username)).findFirst().get();
    }
}