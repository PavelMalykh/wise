package com.api.response;

import com.google.gson.Gson;

import java.util.List;
import java.util.Objects;

public class ResponseError {

    private Boolean success;
    private List<String> message = null;

    public Boolean getSuccess() {
        return success;
    }

    public ResponseError setSuccess(Boolean success) {
        this.success = success;
        return this;
    }

    public List<String> getMessage() {
        return message;
    }

    public ResponseError setMessage(List<String> message) {
        this.message = message;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResponseError that = (ResponseError) o;
        return Objects.equals(success, that.success) && Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(success, message);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}