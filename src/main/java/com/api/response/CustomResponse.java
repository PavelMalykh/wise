package com.api.response;

import io.qameta.allure.Step;
import io.restassured.common.mapper.TypeRef;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;

import java.lang.reflect.Type;

public class CustomResponse {

    @Step("Get response body")
    public static <T> T getResponseBodyAs(Response response, Class<T> tClass) {
        return response.getBody().as(tClass, ObjectMapperType.GSON);
    }

    @Step("Get response body")
    public static <T> T getResponseBodyAs(Response response, Class<T>[] tClass) {
        return response.getBody().as((Type) tClass.getClass(), ObjectMapperType.GSON);
    }

    @Step("Get response body")
    public static <T> T getResponseBodyAs(Response response, TypeRef<T> ref) {
        return response.getBody().as(ref);
    }
}