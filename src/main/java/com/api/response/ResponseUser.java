package com.api.response;

import com.api.data.User;
import com.google.gson.Gson;

import java.util.Objects;

public class ResponseUser {

    private Boolean success;
    private User details;
    private String message;

    public Boolean getSuccess() {
        return success;
    }

    public ResponseUser setSuccess(Boolean success) {
        this.success = success;
        return this;
    }

    public User getDetails() {
        return details;
    }

    public ResponseUser setDetails(User details) {
        this.details = details;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ResponseUser setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResponseUser that = (ResponseUser) o;
        return Objects.equals(success, that.success) && Objects.equals(details, that.details) && Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(success, details, message);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}