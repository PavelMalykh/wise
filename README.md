You can use tests in parallel by adding param -Psuite="Parallel", also you can configure thread amount by adding param -Pthreads="2" (you can add any integer number) into 'arguments'. In case of no additional params, the test will start with default configuration (Single thread (with a single suite)).
Also, I added params -Pprotocol and -Phost to configure a protocol and a host, for example: -Pprotocol="https" and -Phost="mytest.com"

To see an allure report after passing the automated tests, you need to follow the instruction:

Run tests (when tests finish you will see a new folder 'build' inside a project, inside 'build' folder you will see 'allure-results' folder)
Change your directory into 'build' and run a command 'allure serve' (report will be formed automatically)